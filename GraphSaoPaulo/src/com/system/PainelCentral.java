package com.system;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PainelCentral extends JPanel {
	private class Pair {
		Vertice next;
		int idx;
		Pair prev;

		public Pair(Vertice next, int idx) {
			this.next = next;
			this.idx = idx;
			prev = null;
		}
	}

	int origem, destino;
	boolean bfs = false, dfs = false, bidirecional = false;
	private static final long serialVersionUID = 1L;
	private List<Vertice> vertices = new ArrayList<Vertice>();
	private ArrayList<ArrayList<Pair>> graph = new ArrayList<ArrayList<Pair>>();
	private JTextField origemTextField;
	private JTextField destinoTextField;

	public PainelCentral() {
		setBounds(10, 10, 679, 530);
		setBorder(BorderFactory.createBevelBorder(1));
		setLayout(null);

		JButton bfsButton = new JButton("BFS");
		bfsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				origem = Integer.parseInt(origemTextField.getText());
				destino = Integer.parseInt(destinoTextField.getText());
				bfs = true;
				repaint();
			}
		});
		bfsButton.setBounds(416, 458, 89, 23);
		add(bfsButton);

		origemTextField = new JTextField();
		origemTextField.setBounds(350, 423, 44, 20);
		add(origemTextField);
		origemTextField.setColumns(10);

		destinoTextField = new JTextField();
		destinoTextField.setColumns(10);
		destinoTextField.setBounds(461, 423, 44, 20);
		add(destinoTextField);

		JLabel origemLabel = new JLabel("Origem:");
		origemLabel.setBounds(294, 426, 46, 14);
		add(origemLabel);

		JLabel destinoLabel = new JLabel("Destino:");
		destinoLabel.setBounds(405, 426, 46, 14);
		add(destinoLabel);

		JButton dfsButton = new JButton("DFS");
		dfsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				origem = Integer.parseInt(origemTextField.getText());
				destino = Integer.parseInt(destinoTextField.getText());
				dfs = true;
				repaint();
			}
		});
		dfsButton.setBounds(416, 496, 89, 23);
		add(dfsButton);

		JButton bidirecionalButton = new JButton("Bidirect");
		bidirecionalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				origem = Integer.parseInt(origemTextField.getText());
				destino = Integer.parseInt(destinoTextField.getText());
				bidirecional = true;
				repaint();
			}
		});
		bidirecionalButton.setBounds(519, 458, 89, 23);
		add(bidirecionalButton);
		setVisible(true);
		for (int i = 0; i < 25; i++) {
			graph.add(new ArrayList<PainelCentral.Pair>());
		}
		initVertices();
		fillGraph();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		long start = System.currentTimeMillis();
		int iterations = 0;
		for (int i = 0; i < 24; i++) {
			int x1 = vertices.get(i).getX();
			int y1 = vertices.get(i).getY();
			for (int j = 0; j < graph.get(i).size(); j++) {
				int x2 = vertices.get(graph.get(i).get(j).idx).getX();
				int y2 = vertices.get(graph.get(i).get(j).idx).getY();
				g2d.drawLine(x1 + 25, y1 + 25, x2 + 25, y2 + 25);
			}
		}

		if (bfs == true) {
			boolean visited[] = new boolean[25];
			Stroke stroke = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
			g2d.setStroke(stroke);
			int from = origem, to = destino;
			Queue<Pair> ss = new LinkedList<Pair>();
			ss.offer(new Pair(vertices.get(from), from));
			List<Pair> path = new ArrayList<Pair>();
			for (int i = 0; i < 25; i++) {
				path.add(new Pair(null, i));
			}
			visited[from] = true;
			while (ss != null && !ss.isEmpty()) {
				Pair topo = ss.peek();
				ss.poll();
				for (int i = 0; i < graph.get(topo.idx).size(); i++) {
					if (!visited[graph.get(topo.idx).get(i).idx]) {
						iterations++;
						ss.add(new Pair(graph.get(topo.idx).get(i).next, graph.get(topo.idx).get(i).idx));
						visited[graph.get(topo.idx).get(i).idx] = true;
						path.get(graph.get(topo.idx).get(i).idx).prev = path.get(topo.idx);
						if (graph.get(topo.idx).get(i).idx == to) {
							ss = null;
							break;
						}
					}
				}
			}
			g2d.setColor(Color.RED);
			for (Pair i = path.get(to); i != null;) {
				int x1 = vertices.get(i.idx).getX();
				int y1 = vertices.get(i.idx).getY();
				if (i.prev != null) {
					i = i.prev;
					int x2 = vertices.get(i.idx).getX();
					int y2 = vertices.get(i.idx).getY();
					g2d.drawLine(x1 + 25, y1 + 25, x2 + 25, y2 + 25);
				} else {
					break;
				}
			}
			bfs = false;
			System.out.println("Time: " + (System.currentTimeMillis() - start) + ", it: "+iterations);
		} else if (dfs == true) {
			boolean visited[] = new boolean[25];
			Stroke stroke = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
			g2d.setStroke(stroke);
			int from = origem, to = destino;
			Stack<Pair> ss = new Stack<Pair>();
			ss.push(new Pair(vertices.get(from), from));
			List<Pair> path = new ArrayList<Pair>();
			for (int i = 0; i < 25; i++) {
				path.add(new Pair(null, i));
			}
			visited[from] = true;
			while (!ss.isEmpty()) {
				Pair topo = ss.peek();
				ss.pop();
				for (int i = 0; i < graph.get(topo.idx).size(); i++) {
					if (!visited[graph.get(topo.idx).get(i).idx]) {
						ss.add(new Pair(graph.get(topo.idx).get(i).next, graph.get(topo.idx).get(i).idx));
						visited[graph.get(topo.idx).get(i).idx] = true;
						path.get(graph.get(topo.idx).get(i).idx).prev = path.get(topo.idx);
						iterations++;
					}
				}
			}
			g2d.setColor(Color.RED);
			for (Pair i = path.get(to); i != null;) {
				int x1 = vertices.get(i.idx).getX();
				int y1 = vertices.get(i.idx).getY();
				if (i.prev != null) {
					i = i.prev;
					int x2 = vertices.get(i.idx).getX();
					int y2 = vertices.get(i.idx).getY();
					g2d.drawLine(x1 + 25, y1 + 25, x2 + 25, y2 + 25);
				} else {
					break;
				}
			}
			dfs = false;
			System.out.println("Time: " + (System.currentTimeMillis() - start) + ", it: "+iterations);
		} else if (bidirecional == true) {
			boolean visitedo[] = new boolean[25];
			boolean visitedd[] = new boolean[25];
			Stroke stroke = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
			g2d.setStroke(stroke);
			int from = origem, to = destino;
			Queue<Pair> sso = new LinkedList<Pair>();
			sso.offer(new Pair(vertices.get(from), from));
			List<Pair> patho = new ArrayList<Pair>();
			List<Pair> pathd = new ArrayList<Pair>();
			Queue<Pair> ssd = new LinkedList<Pair>();
			ssd.offer(new Pair(vertices.get(to), to));
			for (int i = 0; i < 25; i++) {
				patho.add(new Pair(null, i));
				pathd.add(new Pair(null, i));
			}
			visitedo[from] = true;
			visitedd[to] = true;
			boolean queueing = true;
			while (queueing) {
				if (!sso.isEmpty()) {
					queueing = true;
					Pair topo = sso.peek();
					sso.poll();
					for (int i = 0; i < graph.get(topo.idx).size(); i++) {
						if (!visitedo[graph.get(topo.idx).get(i).idx]) {
							iterations++;
							sso.add(new Pair(graph.get(topo.idx).get(i).next, graph.get(topo.idx).get(i).idx));
							visitedo[graph.get(topo.idx).get(i).idx] = true;
							patho.get(graph.get(topo.idx).get(i).idx).prev = patho.get(topo.idx);
							if (visitedd[graph.get(topo.idx).get(i).idx] == true) {
								queueing = false;
								to = from = graph.get(topo.idx).get(i).idx;
								break;
							}
						}
					}
				}
				if (!ssd.isEmpty() && !queueing) {
					queueing = true;
					Pair topo = ssd.peek();
					ssd.poll();
					for (int i = 0; i < graph.get(topo.idx).size(); i++) {
						if (!visitedd[graph.get(topo.idx).get(i).idx]) {
							iterations++;
							ssd.add(new Pair(graph.get(topo.idx).get(i).next, graph.get(topo.idx).get(i).idx));
							visitedd[graph.get(topo.idx).get(i).idx] = true;
							pathd.get(graph.get(topo.idx).get(i).idx).prev = pathd.get(topo.idx);
							if (visitedo[graph.get(topo.idx).get(i).idx] == true) {
								queueing = false;
								to = from = graph.get(topo.idx).get(i).idx;
								break;
							}
						}
					}
				}
			}
			g2d.setColor(Color.RED);

			for (Pair i = patho.get(to); i != null;) {
				int x1 = vertices.get(i.idx).getX();
				int y1 = vertices.get(i.idx).getY();
				if (i.prev != null) {
					i = i.prev;
					int x2 = vertices.get(i.idx).getX();
					int y2 = vertices.get(i.idx).getY();
					g2d.drawLine(x1 + 25, y1 + 25, x2 + 25, y2 + 25);
				} else {
					break;
				}
			}

			for (Pair i = pathd.get(from); i != null;) {
				int x1 = vertices.get(i.idx).getX();
				int y1 = vertices.get(i.idx).getY();
				if (i.prev != null) {

					i = i.prev;
					int x2 = vertices.get(i.idx).getX();
					int y2 = vertices.get(i.idx).getY();
					g2d.drawLine(x1 + 25, y1 + 25, x2 + 25, y2 + 25);
				} else {
					break;
				}
			}
			System.out.println("Time: " + (System.currentTimeMillis() - start) + ", it: "+iterations);
			bidirecional = false;
		}
		
		g2d.dispose();
	}

	public void fillGraph() {
		graph.get(0).add(new Pair(vertices.get(1), 1));
		graph.get(1).add(new Pair(vertices.get(2), 2));
		graph.get(2).add(new Pair(vertices.get(3), 3));
		graph.get(3).add(new Pair(vertices.get(4), 4));
		graph.get(4).add(new Pair(vertices.get(5), 5));
		graph.get(5).add(new Pair(vertices.get(6), 6));
		graph.get(6).add(new Pair(vertices.get(7), 7));
		graph.get(7).add(new Pair(vertices.get(8), 8));
		graph.get(8).add(new Pair(vertices.get(9), 9));
		graph.get(9).add(new Pair(vertices.get(10), 10));
		graph.get(10).add(new Pair(vertices.get(11), 11));
		graph.get(11).add(new Pair(vertices.get(12), 12));
		graph.get(12).add(new Pair(vertices.get(13), 13));
		graph.get(13).add(new Pair(vertices.get(14), 14));
		graph.get(14).add(new Pair(vertices.get(15), 15));
		graph.get(15).add(new Pair(vertices.get(16), 16));
		graph.get(16).add(new Pair(vertices.get(17), 17));
		graph.get(17).add(new Pair(vertices.get(18), 18));
		graph.get(18).add(new Pair(vertices.get(19), 19));
		graph.get(19).add(new Pair(vertices.get(20), 20));
		graph.get(20).add(new Pair(vertices.get(21), 21));
		graph.get(21).add(new Pair(vertices.get(22), 22));
		graph.get(22).add(new Pair(vertices.get(23), 23));

		graph.get(9).add(new Pair(vertices.get(22), 22));
		graph.get(22).add(new Pair(vertices.get(9), 9));

		graph.get(21).add(new Pair(vertices.get(8), 8));
		graph.get(8).add(new Pair(vertices.get(21), 21));

		graph.get(6).add(new Pair(vertices.get(14), 14));
		graph.get(14).add(new Pair(vertices.get(6), 6));

		graph.get(11).add(new Pair(vertices.get(17), 17));
		graph.get(17).add(new Pair(vertices.get(11), 11));

		graph.get(11).add(new Pair(vertices.get(16), 16));
		graph.get(16).add(new Pair(vertices.get(11), 11));

		graph.get(11).add(new Pair(vertices.get(4), 4));
		graph.get(4).add(new Pair(vertices.get(11), 11));

		graph.get(11).add(new Pair(vertices.get(6), 6));
		graph.get(6).add(new Pair(vertices.get(11), 11));

		graph.get(1).add(new Pair(vertices.get(0), 0));
		graph.get(2).add(new Pair(vertices.get(1), 1));
		graph.get(3).add(new Pair(vertices.get(2), 2));
		graph.get(4).add(new Pair(vertices.get(3), 3));
		graph.get(5).add(new Pair(vertices.get(4), 4));
		graph.get(6).add(new Pair(vertices.get(5), 5));
		graph.get(7).add(new Pair(vertices.get(6), 6));
		graph.get(8).add(new Pair(vertices.get(7), 7));
		graph.get(9).add(new Pair(vertices.get(8), 8));
		graph.get(10).add(new Pair(vertices.get(9), 9));
		graph.get(11).add(new Pair(vertices.get(10), 10));
		graph.get(12).add(new Pair(vertices.get(11), 11));
		graph.get(13).add(new Pair(vertices.get(12), 12));
		graph.get(14).add(new Pair(vertices.get(13), 13));
		graph.get(15).add(new Pair(vertices.get(14), 14));
		graph.get(16).add(new Pair(vertices.get(15), 15));
		graph.get(17).add(new Pair(vertices.get(16), 16));
		graph.get(18).add(new Pair(vertices.get(17), 17));
		graph.get(19).add(new Pair(vertices.get(18), 18));
		graph.get(20).add(new Pair(vertices.get(19), 19));
		graph.get(21).add(new Pair(vertices.get(20), 20));
		graph.get(22).add(new Pair(vertices.get(21), 21));
		graph.get(23).add(new Pair(vertices.get(22), 22));

	}

	public void initVertices() {
		BufferedImage bf;
		try {
			bf = ImageIO.read(Main.class.getResource("/Vertice.png"));

			Vertice vertice = new Vertice("0", bf);
			vertice.setLocation(34, 25);
			vertices.add(vertice);

			Vertice vertice_1 = new Vertice("1", bf);
			vertice_1.setBounds(156, 60, 80, 50);
			vertices.add(vertice_1);

			Vertice vertice_2 = new Vertice("2", bf);
			vertice_2.setBounds(59, 120, 80, 50);
			vertices.add(vertice_2);

			Vertice vertice_3 = new Vertice("3", bf);
			vertice_3.setBounds(188, 153, 80, 50);
			vertices.add(vertice_3);

			Vertice vertice_4 = new Vertice("4", bf);
			vertice_4.setBounds(260, 37, 80, 50);
			vertices.add(vertice_4);

			Vertice vertice_5 = new Vertice("5", bf);
			vertice_5.setBounds(299, 173, 80, 50);
			vertices.add(vertice_5);

			Vertice vertice_6 = new Vertice("6", bf);
			vertice_6.setBounds(373, 48, 80, 50);
			vertices.add(vertice_6);

			Vertice vertice_7 = new Vertice("7", bf);
			vertice_7.setBounds(389, 135, 80, 50);
			vertices.add(vertice_7);

			Vertice vertice_8 = new Vertice("8", bf);
			vertice_8.setBounds(496, 25, 80, 50);
			vertices.add(vertice_8);

			Vertice vertice_9 = new Vertice("9", bf);
			vertice_9.setBounds(477, 120, 80, 50);
			vertices.add(vertice_9);

			Vertice vertice_10 = new Vertice("10", bf);
			vertice_10.setBounds(477, 191, 80, 50);
			vertices.add(vertice_10);

			Vertice vertice_11 = new Vertice("11", bf);
			vertice_11.setBounds(389, 248, 80, 50);
			vertices.add(vertice_11);

			Vertice vertice_12 = new Vertice("12", bf);
			vertice_12.setBounds(260, 270, 80, 50);
			vertices.add(vertice_12);

			Vertice vertice_13 = new Vertice("13", bf);
			vertice_13.setBounds(156, 248, 80, 50);
			vertices.add(vertice_13);

			Vertice vertice_14 = new Vertice("14", bf);
			vertice_14.setBounds(59, 235, 80, 50);
			vertices.add(vertice_14);

			Vertice vertice_15 = new Vertice("15", bf);
			vertice_15.setBounds(34, 324, 80, 50);
			vertices.add(vertice_15);

			Vertice vertice_16 = new Vertice("16", bf);
			vertice_16.setBounds(188, 362, 80, 50);
			vertices.add(vertice_16);

			Vertice vertice_17 = new Vertice("17", bf);
			vertice_17.setBounds(334, 324, 80, 50);
			vertices.add(vertice_17);

			Vertice vertice_18 = new Vertice("18", bf);
			vertice_18.setBounds(525, 248, 80, 50);
			vertices.add(vertice_18);

			Vertice vertice_19 = new Vertice("19", bf);
			vertice_19.setBounds(465, 339, 80, 50);
			vertices.add(vertice_19);

			Vertice vertice_20 = new Vertice("20", bf);
			vertice_20.setBounds(599, 301, 80, 50);
			vertices.add(vertice_20);

			Vertice vertice_21 = new Vertice("21", bf);
			vertice_21.setBounds(590, 202, 80, 50);
			vertices.add(vertice_21);

			Vertice vertice_22 = new Vertice("22", bf);
			vertice_22.setBounds(589, 120, 80, 50);
			vertices.add(vertice_22);

			Vertice vertice_23 = new Vertice("23", bf);
			vertice_23.setBounds(586, 37, 80, 50);
			vertices.add(vertice_23);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < vertices.size(); i++) {
			add(vertices.get(i));
		}
	}
}
