package com.system;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Vertice extends JLabel {

	private static final long serialVersionUID = 1L;

	private String state;
	private BufferedImage verticepng;

	public Vertice(String state, BufferedImage png) {
		this.verticepng = png;
		this.state = state;
		this.setIcon(new ImageIcon());
		this.setText(state);
		this.setBounds(0, 0, 80, 50);

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.drawImage(verticepng, 0, 0, null);
		g2d.drawString(state, 20, 30);
	}
}
